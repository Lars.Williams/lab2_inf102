package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		// adding out of bounds throws
		if (index < 0 || index >= n) {
	        throw new IndexOutOfBoundsException("Index out of bounds: " + index);
	    }
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {
		// adding out of bounds throws
	    if (index < 0 || index > n) {
	        throw new IndexOutOfBoundsException("Index out of bounds: " + index);
	    }
		
		
	    // When the array is full its size increases by one
	    if (n == elements.length) {
	        elements = Arrays.copyOf(elements, elements.length + 1);
	    }

	    // Shifting elements to the right for the inserted element
	    for (int i = n; i > index; i--) {
	        elements[i] = elements[i - 1];
	    }

	    // Adding the new element
	    elements[index] = element;
	    n++; // Incrementing the size
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}